---
title: "Generating text using an LSTM : Harry Prompter"
author: Arthur Amalvy
---

# Introduction

Text generation is a challenging task in NLP. Despite this, it remains an important (and fun) one, and countless researchers have worked on the subject. 

The usual method to tackle such a problem consist of using a language model to predict the next word of a sentence repeatedly. Traditional RNN (Recurrent Neural Networks) have been used for this task, later followed by LSTM and GRU. While they are not considered state of the art anymore, we decided to take a look at it and to try generating meaningful sentences with them.

We chose to use Pytorch for our implementation. You can create a python virtual environnement to test the program :

```sh
python -m venv /env
source env/bin/activate
pip install -r requirements.txt
```


# Quick Start

You can start training a model using (because it is too big, no model is included) :

```sh
python3 main.py --update-model --update-word-embedding
```

To only generate stories (you need already trained model, and a corpus), just launch :

```sh
python3 main.py --output "./out/"
```

If you are lost, you can alwayse use :

```sh
python3 main.py --help
```


# Training and General Structures

We train our system using all 7 harry potters books.

First, we train the classic *Word2Vec* word embedding on them, in the hope that the obtained representations will allow our system to use transfer learning.

We then train a LSTM neural network, followed by a linear layer. As input, we feed the network sentences obtained in harry potters book (tokenized using *nltk*), and try to make him guess the same sentence shifted one token to the right. We also tried to make him guess only the next word, but it seemed to affect the training, yielding bad results. The corresponding pytorch module roughly looks like this :

```python
class LSTMModel(torch.nn.Module):
    
    def __init__(self, parameters):
        # ----------------------
        # assign parameters here
        # ...
        # ----------------------
        self.embedding = torch.nn.Embedding(embedding_parameters)
        self.lstm = torch.nn.LSTM(lstm_parameters)
        self.linear = torch.nn.Linear(linear_parameters)

    def forward(self, sequence, hidden_state, memory_state):
        embedded = self.embedding(seq)
        output, state = self.lstm(embedded, (hidden_state, memory_state))
        pred = self.dense(output)
        return pred, state
```

As the core of our model is a LSTM, which means it does not only generate predictions, but also a hidden state and a memory state, that must be reused to generate sequences.

Using pytorch, the training (not the generation though) of the model is compatible with GPU-accelerated architectures. This means you can use services such as *Google Colaboratory* to train the model. As an example, we trained the latest model using the following code :

```python
import argparse
from main import train_we_model, train

args = argparse.Namespace(
    corpus_glob="/../harry-prompter-datas/*", # relative path to corpus files
    model_path="/models/model.pth",
    gradient_norm=5,
    hidden_size=256,
    verbose=True,
    update_word_embedding=True,
    update_model=True,
    learning_rate=0.01,
    epoch_nums=50,
    batch_size=32,
    seq_size=32,
    layer_number=2
)

we_model = train_we_model(args)
model = train(args, we_model)
```

The word embedding and torch models will placed under the *models* folder.

You can also train the model using the command line :

```sh
python3 main.py --update-word-embedding --update-model
```

you can use `python3 main.py --help` to see all the possible flags. if *update-word-embedding* is **False**, the script will search in the *models* folder for an already generated word embedding file named *word_embedding.model* by default. Similarly, if the *update-model* is **False**, the script will search a torch model (default : *"./models/model.pth"*).

After training, the script will generate stories using all files in the *"./in"* folder. You can also generate example manually using the *generate* function.


# Text Generation

Generation is the fun part. We take an input sequence and predict the first next word. Doing so, we initialize our model hidden and memory states. We then call the model repeatdly, using the obtained word, hidden state and memory state as input at each step. In pseudo python code, it may look like this :

```python
def next_word(sequence, hidden_state, memory_state):
    predictions, (hidden_state, memory_state) = model(sequence, hidden_state, memory_state)
    return argmax(predictions[-1]), (hidden_state, memory_state)

hidden_state, memory_state = model.init_hidden_state()
word, (hidden_state, memory_state) = next_word(sequence, hidden_state, memory_state)
sequence.append(word)
while stop_condition:
    word, (hidden_state, memory_state) = next_word(word, hidden_state, memory_state)
    sequence.append(word)
print(sequence)
```

However, this approach won't yield very good results. As the most likely word is predicted at each step, we can easily get stuck in a loop where the same word is repeating over and over again. to prevent that, we can add a bit of randomness to our output. Here are two different ways to achieve that :

* Pick randomly between the $k$ most probable results
* Sample all results using a *softmax* function, and randomly draw a word from it. This is the approach we found the most succes with


# Results

You can find the generated stories under the *./out/* folder.

The results aren't extremely good. The model shows understanding of basic sentences structure (it can use punctuation for example), but still generates a lot of incoherent utterances. Doing error analysis, it seemed that the network is underfitted, and more training might have been beneficial. Also, it seems that more LSTM layers yield to better results, but adding layer means increased training time, which we already lacked.

Overall, the main problem has been the training costs of the LSTM. To mitigate this, we could have used a GRU model, or a state-of-the-art approach using transformers.