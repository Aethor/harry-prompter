import os, argparse, random, re
from glob import glob

from gensim.models import Word2Vec
import torch
from torch.nn.utils.rnn import pad_sequence
import nltk

from utils import TextIterator, globalize_path
from model import LSTMModel


def get_argument_parser():
    args_parser = argparse.ArgumentParser()
    args_parser.add_argument(
        "-cg",
        "--corpus-glob",
        type=str,
        default="/corpus/*",
        help="corpus files globbing. (default : /corpus/*)",
    )
    args_parser.add_argument(
        "-ig",
        "--input-glob",
        type=str,
        default="/in/*",
        help="input files globbing (defaul : /in/*)",
    )
    args_parser.add_argument(
        "-mp",
        "--model-path",
        type=str,
        default="/models/model.pth",
        help="path to the torch model (default : /models/model.pth)",
    )
    args_parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="if True, will print more debugging messages (default : False)",
    )
    args_parser.add_argument(
        "-gn",
        "--gradient-norm",
        type=int,
        default=5,
        help="Maximum gradient norm before clipping (default : 5)",
    )
    args_parser.add_argument(
        "-en",
        "--epochs-num",
        type=int,
        default=50,
        help="Number of epoch for training (default : 50)",
    )
    args_parser.add_argument(
        "-uw",
        "--update-word-embedding",
        action="store_true",
        help="if True, update the word embedding regardless of storage (default : False)",
    )
    args_parser.add_argument(
        "-um",
        "--update-model",
        action="store_true",
        help="if True, update the language model regardless of storage (default : False)",
    )
    args_parser.add_argument(
        "-bz", "--batch-size", type=int, default=16, help="batch size (default : 16)"
    )
    args_parser.add_argument(
        "-sz", "--seq-size", type=int, default=32, help="sequence size (default : 32)"
    )
    args_parser.add_argument(
        "-hz",
        "--hidden-size",
        type=int,
        default=256,
        help="Size of the LSTM's hidden state (default : 256)",
    )
    args_parser.add_argument(
        "-lr",
        "--learning-rate",
        type=float,
        default=0.01,
        help="Learning rate of the model (default : 0.01)",
    )
    args_parser.add_argument(
        "-ln", "--layer-number", type=int, default=8, help="Number of layer at training"
    )
    args_parser.add_argument(
        "-s",
        "--stability",
        type=int,
        default=10,
        help="Generation parameter. A lower stability means more chaotic outputs, while a higher one leads to more stable but repeating outputs (default : 10)",
    )
    args_parser.add_argument(
        "-o",
        "--output",
        type=str,
        default=None,
        help="output directory for the generated stories. default is None, meaning stories will be written to standard output.",
    )
    args_parser.description = """
            HARRY PROMPTER :
            A (semi-functional) deep learning text-generation model
        """
    return args_parser


def train_we_model(args):
    if not args.update_word_embedding and os.path.isfile(
        globalize_path("/models/words_embedding.model")
    ):
        if args.verbose:
            print("loading model from /models/word_embedding.model...")
        we_model = Word2Vec.load(globalize_path("/models/words_embedding.model"))
    else:
        if args.verbose:
            print("creating word embedding model...")
        we_model = Word2Vec(
            TextIterator(glob(globalize_path(args.corpus_glob)), splitted=True),
            min_count=1,
        )
        we_model.save(globalize_path("/models/words_embedding.model"))

    return we_model


def w_to_idx(we_model, word):
    return we_model.wv.vocab.get(word).index


def idx_to_w(we_model, idx):
    for key, value in we_model.wv.vocab.items():
        if value.index == idx:
            return key


def batches(we_model, text_iter, seq_size: int, batch_size: int):
    for text in text_iter:
        splitted = nltk.word_tokenize(text)
        out_splitted = splitted
        out_splitted[:-1] = splitted[1:]
        out_splitted[-1] = splitted[0]

        batch_nb = len(splitted) // (seq_size * batch_size)
        for i in range(batch_nb):
            in_seq_batch = torch.zeros(seq_size, batch_size, dtype=torch.long)
            out_seq_batch = torch.zeros(seq_size, batch_size, dtype=torch.long)
            for j in range(batch_size):
                lindex = i * (seq_size * batch_size) + j * seq_size
                rindex = lindex + seq_size
                try:
                    in_seq = [w_to_idx(we_model, w) for w in splitted[lindex:rindex]]
                    out_seq = [
                        w_to_idx(we_model, w) for w in out_splitted[lindex:rindex]
                    ]
                    in_seq_batch[:, j] = torch.LongTensor(in_seq)
                    out_seq_batch[:, j] = torch.LongTensor(out_seq)
                except Exception as e:
                    if args.verbose:
                        print(e)
                        print("Error : skipped a sentence")
                    continue
            yield (in_seq_batch, out_seq_batch)


def train(args, we_model):

    if torch.cuda.is_available():
        if args.verbose:
            print("Using CUDA device")
        device = torch.device("cuda")
    else:
        if args.verbose:
            print("Using CPU device")
        device = torch.device("cpu")

    if not args.update_model and os.path.isfile(globalize_path(args.model_path)):
        model = LSTMModel(
            we_model.vector_size,
            args.hidden_size,
            args.batch_size,
            args.layer_number,
            we_model.wv.vectors,
        )
        model.load_state_dict(
            torch.load(globalize_path(args.model_path), map_location=device),
            strict=False,
        )
        return model

    model = LSTMModel(
        we_model.vector_size,
        args.hidden_size,
        args.batch_size,
        args.layer_number,
        we_model.wv.vectors,
    )

    loss_fn = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(model.parameters(), lr=args.learning_rate)

    model = model.to(device)

    for epoch in range(args.epochs_num):

        print("\nepoch : {} / {}".format(epoch + 1, args.epochs_num))

        hidden_state, memory_state = model.init_hidden_state()
        hidden_state = hidden_state.to(device)
        memory_state = memory_state.to(device)

        i = 0
        for x, y in batches(
            we_model,
            TextIterator(glob(globalize_path(args.corpus_glob))),
            args.seq_size,
            args.batch_size,
        ):
            model.train()

            optimizer.zero_grad()

            x = x.to(device)
            y = y.to(device)

            preds, (hidden_state, memory_state) = model(x, hidden_state, memory_state)

            loss = loss_fn(preds.transpose(1, 2), y)
            loss_value = loss.item()

            hidden_state.detach_()
            memory_state.detach_()

            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), args.gradient_norm)
            optimizer.step()

            print("iteration : {}, loss : {}".format(i, loss_value), end="\r")
            i += 1

        if epoch % 10 == 0:
            # generate(args, model, we_model)
            torch.save(
                model.state_dict(),
                globalize_path(args.model_path) + "epoch" + str(epoch),
            )

    torch.save(model.state_dict(), globalize_path(args.model_path))
    print("done training !")
    return model


def sample(args, predictions):
    """
        :param predictions: (1, vocab_size)
        :returns: index of sampled item
    """
    predictions = torch.softmax(predictions, 1)
    dist = torch.distributions.Multinomial(args.stability, predictions).sample()
    _, idx = dist[0].max(0)
    return idx.item()


def next_word(args, we_model, model, input_words, hidden_state, memory_state):
    device = torch.device("cpu")
    model = model.to(device)

    in_seq = torch.zeros(len(input_words), 1, dtype=torch.long)
    idxs = []
    for w in input_words:
        try:
            idxs.append(w_to_idx(we_model, w))
        except Exception:
            print("unknown word : " + str(w))
            idxs.append(0)
    in_seq[:, 0] = torch.LongTensor(idxs)

    in_seq = in_seq.to(device)
    hidden_state = hidden_state.to(device)
    memory_state = memory_state.to(device)

    preds, (hidden_state, memory_state) = model(in_seq, hidden_state, memory_state)

    return (idx_to_w(we_model, sample(args, preds[-1])), (hidden_state, memory_state))


def generate(args, model, we_model, min_size=200):
    outs = []
    for filename in glob(globalize_path(args.input_glob)):
        with open(filename) as f:
            seq = f.read()
            seq = nltk.word_tokenize(seq)
            hidden_state, memory_state = model.init_hidden_state(batch_size=1)
            model.eval()

            w, (hidden_state, memory_state) = next_word(
                args, we_model, model, seq, hidden_state, memory_state
            )
            for i in range(min_size):
                w, (hidden_state, memory_state) = next_word(
                    args, we_model, model, [w], hidden_state, memory_state
                )
                seq.append(w)

            while not w == "." and not w[-1] == ".":
                w, (hidden_state, memory_state) = next_word(
                    args, we_model, model, [w], hidden_state, memory_state
                )
                seq.append(w)

            out = " ".join(seq)
            outs.append(out)
    return outs


if __name__ == "__main__":
    args = get_argument_parser().parse_args()
    we_model = train_we_model(args)
    model = train(args, we_model)
    outs = generate(args, model, we_model)

    if args.output is None:
        for out in outs:
            print(out, end="\n\n")
    else:
        for i, out in enumerate(outs):
            with open("{}story{}.txt".format(args.output, i), "w") as f:
                f.write(out)
