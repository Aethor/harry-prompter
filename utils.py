import os
from typing import List, Generator

import nltk

nltk.download("punkt")

project_root = os.path.dirname(os.path.abspath(__file__))


def globalize_path(path: str) -> str:
    return project_root + path


class TextIterator:
    def __init__(self, filenames: List[str], splitted=False):
        self.filenames = filenames
        self.generator = self.text_generator()
        self.splitted = splitted

    def __iter__(self):
        self.generator = self.text_generator()
        return self

    def __next__(self):
        res = next(self.generator)
        if res is None:
            raise StopIteration
        return res

    def text_generator(self) -> Generator:
        for filename in self.filenames:
            with open(filename) as f:
                if self.splitted:
                    yield nltk.word_tokenize(f.read())
                else:
                    yield f.read()
        return None
