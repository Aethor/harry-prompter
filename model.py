import torch
from torch import nn


class LSTMModel(nn.Module):
    def __init__(self, input_size, hidden_size, batch_size, num_layers, emb_vectors):
        super(LSTMModel, self).__init__()

        self.input_size = input_size
        self.hidden_size = hidden_size
        self.batch_size = batch_size
        self.num_layers = num_layers

        self.embedding = nn.Embedding.from_pretrained(torch.FloatTensor(emb_vectors))
        self.lstm = nn.LSTM(input_size, hidden_size, self.num_layers)
        self.dense = nn.Linear(self.hidden_size, len(emb_vectors))

    def init_hidden_state(self, batch_size=None):
        if batch_size is None:
            batch_size = self.batch_size
        return (
            torch.zeros(self.num_layers, batch_size, self.hidden_size),
            torch.zeros(self.num_layers, batch_size, self.hidden_size),
        )

    def forward(self, seq, hidden_state, memory_state):
        """
            :param seq: (sequence_size, batch_size)
            :param hidden_state: (num_layers, batch_size, hidden_size)
            :param memory_state: (num_layers, batch_size, hidden_size)
        """
        embedded = self.embedding(seq)
        out, state = self.lstm(embedded, (hidden_state, memory_state))
        pred = self.dense(out)
        return pred, state
